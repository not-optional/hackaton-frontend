import React, {useState} from "react";
import {
    Avatar,
    Button,
    Checkbox,
    FormControlLabel,
    FormGroup,
    Grid,
    Link,
    Paper,
    TextField,
    Typography,
    Select,
    MenuItem
} from "@material-ui/core";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {makeStyles} from "@material-ui/core/styles";
import Transport from "./transport.jpg";
import {Link as RouterLink, Redirect} from "react-router-dom";
import config from "../config"
import axios from "axios";
import {displayError} from "../utils/swal";

const useStyles = makeStyles(theme => ({
    root: {
        height: "100vh"
    },
    image: {
        backgroundImage: `url(${Transport})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "30% 50%"
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: "70%",
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));

export default function Signup() {
    const classes = useStyles();
    const [formData, setFormData] = useState({
        carType: 0,
        publiBikeAccess: false
    });
    const [validation, setValidation] = useState({
        username:  "", 
        password: "", 
        confirmPassword: ""
    });

    const [userCreated, setUserCreated] = useState(false);

    const handleChange = (e) =>
        setFormData(
            Object.assign({}, formData, { [e.target.name]: e.target.value })
        );

    const checkboxChange = (e) =>
        setFormData(
            Object.assign({}, formData, { [e.target.name]: e.target.checked })
        );
    
    const Link1 = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);
    
    const validForm = () => {
        const status = { username:  "", password: "", confirmPassword: "" };
        let valid = true;
        for (let key in validation) {
            if (!formData[key]) {
                status[key] = "This field is required";
                valid = false;
            }
        }

        if (valid && formData.confirmPassword !== formData.password) {
            status.password = status.confirmPassword = "Confirmation password and password must agree";
            valid = false;    
        }
        setValidation(
            Object.assign({}, validation, status)
        );
        return valid;
    }


    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validForm()) return;
        const instance = axios.create({
            baseURL: config.host+config.endpoint,
            timeout: 1500
        });
        try {
            await instance.post("/auth/sign-up", formData);
            setUserCreated(true);
        } catch (e) {
            displayError(e);
        }
    };

    if (userCreated) {
        return <Redirect to="/login"/>
    }

    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={8} className={classes.image} />
            <Grid
                item
                xs={12}
                sm={8}
                md={4}
                component={Paper}
                elevation={6}
                square
                container
                alignContent="center"
            >
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign up
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    autoComplete="given-name"
                                    name="firstName"
                                    variant="outlined"
                                    fullWidth
                                    id="firstName"
                                    label="First Name"
                                    autoFocus
                                    onChange={handleChange}
                                />
                            </Grid>
                            <Grid item xs={12} sm={6}>
                                <TextField
                                    variant="outlined"
                                    fullWidth
                                    id="lastName"
                                    label="Last Name"
                                    name="lastName"
                                    autoComplete="family-name"
                                    onChange={handleChange}
                                />
                            </Grid>
                        </Grid>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="username"
                            label="Username"
                            id="username"
                            autoComplete="username"
                            onChange={handleChange}
                            helperText={validation.username}
                            error={validation.username.length > 0}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            error={validation.password.length > 0}
                            helperText={validation.password}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="confirmPassword"
                            label="Confirm Password"
                            type="password"
                            id="password-confirm"
                            error={validation.confirmPassword.length > 0}
                            helperText={validation.confirmPassword}
                            onChange={handleChange}
                        />
                        <Select 
                            fullWidth
                            variant="outlined" 
                            name="carType" 
                            onChange={handleChange}
                            defaultValue={formData.carType}>
                            <MenuItem value={0}>I don't have a car</MenuItem>
                            <MenuItem value={1}>I have a car</MenuItem>
                            <MenuItem value={2}>I have an electric car</MenuItem>
                        </Select>
                        <FormGroup column="true">
                            <FormControlLabel
                                control={
                                    <Checkbox
                                        color="primary"
                                        name="publiBikeAccess"
                                        onChange={checkboxChange}
                                    />
                                }
                                label="I have a public bike account"
                            />
                        </FormGroup>
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign Up
                        </Button>
                        <Grid item>
                            <Link to="/login" variant="body2" component={Link1}>
                                {"Already have an account? Sign in"}
                            </Link>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}
