import { makeStyles, TextField, MenuItem, Paper, Button} from "@material-ui/core";
import React, {useEffect, useState} from "react";
import {displayError} from "../../utils/swal";
import axios from "axios";

import config from "../../config"

import Autosuggest from 'react-autosuggest';

import match from 'autosuggest-highlight/match';
import parse from 'autosuggest-highlight/parse';

const useStyles = makeStyles(theme => ({
    root: {
        margin: theme.spacing(1, 1),
        flexGrow: 1,
    },
    paper: {
        position: 'absolute',
        zIndex: 2
    },
    container: {
        position: 'relative',
    },
    suggestionsContainerOpen: {
        position: 'absolute',
        zIndex: 1,
        marginTop: theme.spacing(1),
        left: 0,
        right: 0,
    },
    suggestion: {
        display: 'block',
    },
    suggestionsList: {
        margin: 0,
        padding: 0,
        listStyleType: 'none',
    },
    divider: {
        height: theme.spacing(1),
    },
    input: {
        // background: theme.
    }
}));

let timer = null;

function Search({ position, setFromCoordinates, setToCoordinates, loading, queryDirections }) {
    const classes = useStyles();
    const [fromField, setFrom] = useState(null);
    const [toField, setTo] = useState(null);
    const [suggestions, setSuggestions] = useState([]);
    const [state, setState] = useState({
        fromField: '',
        toField: ''
    });
    
    useEffect(() => {
        (async () => {
            if (!fromField) {
                return;
            }
            const instance = axios.create({
                baseURL: config.host+config.endpoint,
            });
            try {
                const { data: { positions } } = await instance.post("/maps/coordinates", {
                    positions: [{locationId: fromField.locationId}]
                });
                const [ coordinates ] = positions;
                setFromCoordinates(coordinates);
            } catch (e) {
                console.error(e)
            }
        })();
    }, [fromField]);
    
    useEffect(() => {
        (async () => {
            if (!toField) {
                return;
            }
            const instance = axios.create({
                baseURL: config.host+config.endpoint,
            });
            try {
                const { data: { positions } } = await instance.post("/maps/coordinates", {
                    positions: [{locationId: toField.locationId}]
                });
                const [ coordinates ] = positions;
                setToCoordinates(coordinates);
            } catch (e) {
                console.error(e)
            }
        })();
    }, [toField]);
    
    
    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            if (!fromField) {
                return displayError(new Error("The FROM location is required"));
            }
            if (!toField) {
                return displayError(new Error("The TO location is required"));
            }
            queryDirections();
        } catch (err) {
            displayError(err);
        }
    };
    
    const handleChange = name => (event, { newValue }) => {
        setState({
            ...state,
            [name]: newValue,
        });
    };
    
    const clearSuggestions = () => setSuggestions([]);
    
    const renderSuggestion = (suggestion, { query, isHighlighted }) => {
        const label = suggestion.label.split(",");
        label.shift();
        const matches = match(label.join(","), query);
        const parts = parse(label.join(","), matches);
        
        return (
            <MenuItem selected={isHighlighted} component={"div"}>
                <div>
                    {parts.map((part, i) => (
                        <span key={part.text+i} style={{ fontWeight: part.highlight ? 800 : 400 }}>
                            {part.text}
                        </span>
                    ))}
                </div>
            </MenuItem>
        );
    };
    
    const getSuggestionValue = (suggestion, isFrom) => {
        if (isFrom) {
            setFrom(suggestion);
        } else {
            setTo(suggestion);
        }
        const label = suggestion.label.split(",");
        label.shift();
        return label.join(",");
    };
    
    const getSuggestions = ({ value }) => {
        if (timer) {
            clearTimeout(timer);
        }
        
        timer = setTimeout(async () => {
            if (value && value.length < 3) {
                return;
            }
            try {
                const instance = axios.create({
                    baseURL: config.host+config.endpoint,
                });
                const {
                    data: { suggestions }
                } = await instance.post(`/maps/suggestions`, {
                    query: value,
                    prox: `${position.lat},${position.lon},5000`,
                    country: "CHE",
                    mapview: "46.054930,8.880415,45.960823,9.008204"
                });
                setSuggestions(suggestions);
            } catch (e) {
                console.error(e);
                if (e.response) {
                    console.error(e.response);
                }
            }
        }, 1000);
    };
    
    const renderInputComponent = (inputProps) => {
        const { classes, inputRef = () => {}, ref, ...other } = inputProps;
        
        return (
            <TextField
                fullWidth
                InputProps={{
                    inputRef: node => {
                        ref(node);
                        inputRef(node);
                    },
                    classes: {
                        input: classes.input,
                    },
                }}
                {...other}
            />
        );
    };
    
    const autosuggestProps = (isFrom) => ({
        renderInputComponent,
        suggestions,
        onSuggestionsFetchRequested: getSuggestions,
        onSuggestionsClearRequested: clearSuggestions,
        getSuggestionValue: (suggestion) => getSuggestionValue(suggestion, isFrom),
        renderSuggestion
    });
    
    return <form className={classes.root} onSubmit={handleSubmit}>
        <Autosuggest
            {...autosuggestProps(true)}
            inputProps={{
                classes,
                id: 'react-autosuggest-simple',
                label: 'From',
                variant: "outlined",
                value: state.fromField,
                onChange: handleChange("fromField"),
            }}
            theme={{
                container: classes.container,
                suggestionsContainerOpen: classes.suggestionsContainerOpen,
                suggestionsList: classes.suggestionsList,
                suggestion: classes.suggestion,
            }}
            renderSuggestionsContainer={options => (
                <Paper {...options.containerProps} className={classes.paper} square>
                    {options.children}
                </Paper>
            )}
        />
        <div className={classes.divider}/>
        <Autosuggest
            {...autosuggestProps(false)}
            inputProps={{
                classes,
                id: 'react-autosuggest-simple',
                label: 'To',
                variant: "outlined",
                value: state.toField,
                onChange: handleChange("toField"),
            }}
            theme={{
                container: classes.container,
                suggestionsContainerOpen: classes.suggestionsContainerOpen,
                suggestionsList: classes.suggestionsList,
                suggestion: classes.suggestion,
            }}
            renderSuggestionsContainer={options => (
                <Paper {...options.containerProps} className={classes.paper} square>
                    {options.children}
                </Paper>
            )}
        />
        <div className={classes.divider}/>
        <Button
            disabled={loading}
            type="submit"
            variant="contained"
            color="primary">
            Find a way
        </Button>
    </form>
    
}

export default Search;
