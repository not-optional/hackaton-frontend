import React, { Fragment } from "react";
import {List, ListItem, ListItemText, Divider, Icon} from "@material-ui/core";
import { createStyles, makeStyles } from "@material-ui/core/styles";

import "./style.sass"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWalking, faBicycle, faBus, faCarBattery, faCar, faExclamationTriangle } from '@fortawesome/free-solid-svg-icons'
import {Tooltip} from "recharts";

const IconTypes = {
    "pedestrian": faWalking,
    "bike": faBicycle,
    "bus": faBus,
    "electric-car": faCarBattery,
    "car": faCar
};

const TransportColors = {
    'pedestrian': "#00a2ff",
    'bike': "#61d836",
    'bus': "#ff9300",
    'car': '#b51800',
    'electric-car': "#cb297b"
};

const useStyles = makeStyles(theme =>
    createStyles({
        // root: {width: '100%'},
        path: { display: "block" },
        innerList: { marginLeft: theme.spacing(1.5)}
    })
);

export default function PathsList({paths, idx, setIdx}) {
    const classes = useStyles();
    const handleClick = index => {
        setIdx(index);
    };
    
    const parseTime = (time) => {
        if (time < 60) {
            return `${Math.round(time)} s`;
        }
        time /= 60;
        if (time < 60) {
            return `${Math.round(time)} m`;
        }
        time /= 60;
        if (time < 24) {
            return `${Math.round(time)} h`;
        }
        time /= 24;
        return `${Math.round(time)} d`;
    };
    
    console.log(paths);
    return (
        <div className={"list-container"}>
            <List>
                {paths.map((path, i) => (
                    <Fragment key={i}>
                        <ListItem
                            className={classes.path}
                            button
                            selected={idx === i}
                            onClick={() => handleClick(i)}
                        >
                            <ListItemText primary={`Option ${i+1}: ${parseTime(path.time)}`} />
                            <div className={"additional-info"}>
                                <span className={"icons"}>
                                    {path.modes.map(x => <FontAwesomeIcon color={TransportColors[x]} icon={IconTypes[x]}/>)}
                                </span>
                                
                                <span className={"warnings"}>
                                    <span className={"raining"}>
                                    
                                    </span>
                                    
                                    {path.isRaining && <span className={"raining"}><Tooltip title={"Showers are expected in the considered area\n" +
                                    ", travel safely at all times and  bring an umbrella!"}><FontAwesomeIcon color={"#ffff00"} icon={faExclamationTriangle}/></Tooltip></span>}
                                   
                                   {(() => {
                                       const p = path.points.find(x => x.bikes_remaining);
                                       if (!p) return null;
                                       const [bikes, e_bikes] = p.bikes_remaining;
                                       return <span className={"bikes-left"}>{`${bikes} Bikes | ${e_bikes} E-Bikes`}</span>;
                                   })()}
                                </span>
                            </div>
                            
                        </ListItem>
                        {idx === i && path.tracks && (
                            <List className={classes.innerList}>
                                {path.tracks.map(el => el.map((segment, j) => (
                                    <ListItem key={j}>
                                        <span
                                            dangerouslySetInnerHTML={{__html: segment.instruction}}
                                        />
                                    </ListItem>
                                )))}
                            </List>
                        )}
                        <Divider/>
                    </Fragment>
                ))}
            </List>
        </div>
    );
}
