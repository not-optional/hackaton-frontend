import React, {Fragment, useEffect, useState} from 'react';
import ReactMapGL, { Marker, LinearInterpolator} from "react-map-gl";
import DeckGL from '@deck.gl/react';
import {LineLayer} from '@deck.gl/layers';
import { makeStyles } from '@material-ui/core/styles';

import config from "../../config";

import "./style.sass"
import {Legend} from "./legend";
import {Popover, Typography} from "@material-ui/core";

const PointTypes = {
    GPS: 0,
    ORIGIN: 1,
    DESTINATION: 2,
    GENERIC: 3
};

const TransportColors = {
    0: [0,162,255],
    1: [97,216,54],
    2: [255,147,0],
    3: [203,41,123],
    4: [181,23,0]
};

const useStyles = makeStyles(theme => ({
    marker: {
        zIndex: 100
    },
    typography: {
        padding: theme.spacing(2),
    },
}));

function Map({ data, currentPos, fromCoordinates, toCoordinates }) {
    const [ viewport, setViewport ] = useState({
        width: '100%',
        height: '100%',
        latitude: 46.010916,
        longitude: 8.956969,
        zoom: 8
    });
    
    
    
    const [ active, setActive ] = useState(PointTypes.GPS);
    
    useEffect(() => {
        if (currentPos) {
            setViewport(Object.assign({}, viewport, { latitude: currentPos.lat, longitude: currentPos.lon, zoom: 14 }));
        }
    }, [currentPos]);
    
    useEffect(() => {
        if (fromCoordinates) {
            setViewport(Object.assign({}, viewport, { latitude: fromCoordinates.lat, longitude: fromCoordinates.lon, zoom: 14 }));
            setActive(PointTypes.ORIGIN);
        }
    }, [fromCoordinates]);
    
    useEffect(() => {
        if (toCoordinates) {
            setViewport(Object.assign({}, viewport, { latitude: toCoordinates.lat, longitude: toCoordinates.lon, zoom: 14 }));
            setActive(PointTypes.DESTINATION);
        }
    }, [toCoordinates]);
    
    
    const dataToLayer = (d, idx) => {
        if (d.length === 0) return;
        const layer = {
            id: 'line-layer-'+idx,
            data: d,
            getWidth: 10,
            getSourcePosition: d => d.sourcePosition,
            getTargetPosition: d => d.targetPosition,
            getColor: d => TransportColors[idx],
        };
        return [new LineLayer(layer)];
    };
    
    console.log(data);
    
    const MapPoint = ({ latitude, longitude, idx }) => {
        const classes = useStyles();
    
        const [anchorEl, setAnchorEl] = useState(null);
        
        const handleClick = (e) => setAnchorEl(e.currentTarget);
        
        const handleClose = (e) => {
            console.log(e);
            setAnchorEl(null);
        };
        
        const open = Boolean(anchorEl);
        const id = open ? 'simple-popoover-'+idx : undefined;
        
        return <Fragment>
            <Marker latitude={latitude} longitude={longitude} className={classes.marker}>
                <div onClick={handleClick} className={"circle-marker waypoint"}/>
            </Marker>
            <Popover
                id={id}
                open={open}
                anchorEl={anchorEl}
                onClose={handleClose}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center'
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center'
                }}
            >
                <Typography className={classes.typography}>Content of some sorts</Typography>
            </Popover>
        </Fragment>
    };
    
    return <DeckGL
        onViewportChange={(vp) => setViewport(vp)}
        transitionDuration={200}
        transitionInterpolator={new LinearInterpolator()}
        initialViewState={viewport}
        controller={true}
        layers={[data.tracks.map((x,i) => dataToLayer(x,i))]}>
        <ReactMapGL
            mapboxApiAccessToken={config.mapbox_api_key}
        >
            <Legend/>
            {currentPos && <Marker latitude={currentPos.lat} longitude={currentPos.lon}>
                <div className={["circle-marker", "gps-pos", active === PointTypes.GPS ? "active" : ""].join(" ")}/>
            </Marker>}
            {fromCoordinates && <Marker latitude={fromCoordinates.lat} longitude={fromCoordinates.lon}>
                <div className={["circle-marker", "origin", active === PointTypes.ORIGIN ? "active" : ""].join(" ")}/>
            </Marker>}
            {toCoordinates && <Marker latitude={toCoordinates.lat} longitude={toCoordinates.lon}>
                <div className={["circle-marker", "destination", active === PointTypes.DESTINATION ? "active" : ""].join(" ")}/>
            </Marker>}
            <div className={"custom-overlay"}>
                {data.points.map((x,i) => <MapPoint key={i} idx={i} latitude={x.position.latitude} longitude={x.position.longitude}/>)}
            </div>
        </ReactMapGL>
    </DeckGL>;
    
    
    
}

export default Map;
