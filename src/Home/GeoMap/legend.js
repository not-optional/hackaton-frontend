import React from "react";

export function Legend() {
    return <div className={"legend"}>
        <div className={"entry"}>
            <span className={"color-mark blue"}/><span>Walking</span>
        </div>
        <div className={"entry"}>
            <span className={"color-mark green"}/><span>Bicycling</span>
        </div>
        <div className={"entry"}>
            <span className={"color-mark orange"}/><span>Public Ride</span>
        </div>
        <div className={"entry"}>
            <span className={"color-mark red"}/><span> Car</span>
        </div>
        <div className={"entry"}>
            <span className={"color-mark pink"}/><span>E-Car</span>
        </div>
    </div>
}
