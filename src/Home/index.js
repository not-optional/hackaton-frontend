import React, {useEffect, useState} from "react";
import {Grid, Paper, Divider, LinearProgress} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import SearchMenu from "./SearchMenu";
import GeoMap from "./GeoMap";
import PathsList from "./PathsList";
import axios from "axios";

import config from "../config"
import {isLoading} from "sweetalert2";

export const TransportTypes = {
    "pedestrian": 0,
    "bike": 1,
    "bus": 2,
    "electric-car": 3,
    "car": 4
};

const useStyles = makeStyles(theme => ({
    root: {
        height: "calc(100vh - 64px)"
    },
    divider: {
        width: "100%",
        height: theme.spacing(0.5)
    },
    progress: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

function Home() {
    const classes = useStyles();
    
    const [position, setPosition] = useState({accuracy: 10000, lat: 46.010916, lon: 8.956969});
    const [fromCoordinates, setFromCoordinates] = useState(null);
    const [toCoordinates, setToCoordinates] = useState(null);
    
    const [loading, setLoading] = useState(false);
    
    const [tracks, setTracks] = useState([]);
    const [idx, setIdx] = useState(0);
    
    
    useEffect(() => {
        navigator.geolocation.getCurrentPosition((p) => {
            const pos = {
                lat: p.coords.latitude,
                lon: p.coords.longitude,
                accuracy: p.coords.accuracy
            };
            setPosition(pos);
        });
    }, []);
    
    const queryDirections = async () => {
        if (!fromCoordinates || !toCoordinates) return;
        const instance = axios.create({
            baseURL: config.host+config.endpoint,
            timeout: 30000
        });
        try {
            setLoading(true);
            const { data } = await instance.post("/maps/directions", {
                "from": fromCoordinates,
                "to": toCoordinates
            });
            console.log(data);
            setTracks(data);
        } catch (e) {
            console.error(e);
        } finally {
            setLoading(false);
        }
    };
    
    useEffect(() => {
        queryDirections();
    }, [fromCoordinates, toCoordinates]);
    
    
    const parseTrack = (data) => {
        const tracks = new Array(5).fill([]);
        const modes = [];
        let x = data;
        let time = 0;
        let distance = 0;
        const points = [];
        let oldMode = "";
        let isRaining;
        while (x) {
            let { segment, mode, next } = x;
            isRaining = Boolean(x.isRaining);
            if (oldMode !== mode) {
                modes.push(mode);
                const point = segment.steps[segment.steps.length - 1];
                if (mode === "bike") {
                    point.bikes_remaining = [x["Bike"], x["E-Bike"]];
                    console.log(point, x);
                }
                points.push(point);
            }
            oldMode = mode;
            time += segment.time;
            distance += segment.length;
            let idx = TransportTypes[mode];
            const steps = [];
            for (let i = 0, j = 1; j < segment.steps.length; i++, j++) {
                const fromPos = segment.steps[i];
                const toPos = segment.steps[j];
                const edge = {
                    sourcePosition: [fromPos.position.longitude, fromPos.position.latitude],
                    targetPosition: [toPos.position.longitude, toPos.position.latitude],
                    instruction: fromPos.instruction
                };
                steps.push(edge);
            }
            tracks[idx] = tracks[idx].concat(steps);
            [x] = next;
        }
        return {
            time, distance, tracks, points, modes, isRaining
        }
    };

    return (
        <Grid container component="main" className={classes.root}>
            <Grid
                item
                xs={12}
                sm={4}
                md={3}
                component={Paper}
                elevation={0}
                container
                alignContent="flex-start"
            >
                <SearchMenu loading={loading} queryDirections={queryDirections} setFromCoordinates={setFromCoordinates} setToCoordinates={setToCoordinates} position={position}/>
                {loading && <div className={classes.progress}><LinearProgress variant={'query'}/></div>}
                <Divider className={classes.divider}/>
                <PathsList paths={tracks.map(x => parseTrack(x))} idx={idx} setIdx={setIdx}/>
            </Grid>
            <Grid
                item
                xs={12}
                sm={8}
                md={9}
                elevation={0}
                container
                alignContent={"flex-start"}
            >
                <GeoMap data={parseTrack(tracks[idx])} fromCoordinates={fromCoordinates} toCoordinates={toCoordinates} currentPos={position}/>
            </Grid>
        </Grid>
    );
}

export default Home;
