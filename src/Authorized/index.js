import React, { Fragment } from "react";
import { Switch, Route, Redirect } from "react-router-dom";
import NotFound from "../NotFound";
import Home from "../Home";
import NavBar from "../components/NavBar";
import User from "../User";

export default function Authorized() {
    if (!localStorage.getItem("username") || !localStorage.getItem("token")) {
        return <Redirect to="/login" />
    }
    return (
        <Fragment>
            <NavBar />
            <Switch>
                <Route exact path="/" component={Home} />
                <Route exact path="/user" component={User} />
                <Route component={NotFound} />
            </Switch>
        </Fragment>
    );
}
