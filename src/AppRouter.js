import React from "react";
import { Switch, Route } from "react-router-dom";
import Login from "./Login";
import Signup from "./Signup";
import Authorized from "./Authorized";
import Logout from "./Logout";

export default function AppRouter() {
    return (
        <Switch>
            <Route exact path="/login" component={Login} />
            <Route exact path="/signup" component={Signup} />
            <Route exact path="/logout" component={Logout} />
            <Route component={Authorized} />
        </Switch>
    );
}
