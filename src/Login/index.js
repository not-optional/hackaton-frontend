import React, {useState} from "react";
import {Avatar, Button, Grid, Link, Paper, TextField, Typography} from "@material-ui/core";
import {Link as RouterLink, Redirect} from "react-router-dom"
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import {makeStyles} from "@material-ui/core/styles";
import Lugano from "./lugano.jpg";
import config from "../config"
import axios from "axios";
import {displayError} from "../utils/swal";

const useStyles = makeStyles(theme => ({
    root: {
        height: "100vh"
    },
    image: {
        backgroundImage: `url(${Lugano})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "30% 50%"
    },
    paper: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center"
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main
    },
    form: {
        width: "70%",
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));

export default function Login() {
    const classes = useStyles();

    const [formData, setFormData] = useState({});
    const [validation, setValidation] = useState({
        username:  "", 
        password: ""
    });
    const [loggedIn, setLoggedIn] = useState(localStorage.getItem("username") && localStorage.getItem("token"));

    const handleChange = (e) => setFormData(
        Object.assign({}, formData, {[e.target.name]: e.target.value})
    );

    const validForm = () => {
        const status = { username:  "", password: "" };
        let valid = true;
        for (let key in validation) {
            if (!formData[key]) {
                status[key] = `${key[0].toUpperCase()}${key.slice(1)} is missing`;
                valid = false;
            }
        }
        setValidation(
            Object.assign({}, validation, status)
        );
        return valid;
    }

    
    const handleSubmit = async (e) => {
        e.preventDefault();
        if (!validForm()) return;
        const instance = axios.create({
            baseURL: config.host+config.endpoint,
            timeout: 1500
        });
        try {
            const {data} = await instance.post("/auth/login", {
                username: formData.username,
                password: formData.password
            });
            localStorage.setItem("username", formData.username);
            localStorage.setItem("token", data.token);
            setLoggedIn(true);
        } catch (e) {
            displayError(e);
        }
    };
    
    const Link1 = React.forwardRef((props, ref) => <RouterLink innerRef={ref} {...props} />);
    
    if (loggedIn) {
        return <Redirect to="/" />
    }

    return (
        <Grid container component="main" className={classes.root}>
            <Grid item xs={false} sm={4} md={8} className={classes.image} />
            <Grid
                item
                xs={12}
                sm={8}
                md={4}
                component={Paper}
                elevation={6}
                square
                container
                alignContent="center"
            >
                <div className={classes.paper}>
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Sign in
                    </Typography>
                    <form className={classes.form} noValidate onSubmit={handleSubmit}>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="username"
                            label="Username"
                            name="username"
                            autoComplete="text"
                            autoFocus
                            helperText={validation.username}
                            error={validation.username.length > 0}
                            onChange={handleChange}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            helperText={validation.password}
                            error={validation.password.length > 0}
                            onChange={handleChange}
                        />
                        <Button
                            type="submit"
                            fullWidth
                            variant="contained"
                            color="primary"
                            className={classes.submit}
                        >
                            Sign In
                        </Button>
                        <Grid item>
                            <Link to="/signup" variant={"body2"} component={Link1}>
                                {"Don't have an account? Sign Up"}
                            </Link>
                        </Grid>
                    </form>
                </div>
            </Grid>
        </Grid>
    );
}
