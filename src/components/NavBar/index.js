import React from "react";
import { Link } from "react-router-dom";
import { AppBar, Toolbar, Button } from "@material-ui/core";
import {
    faSignOutAlt,
    faHome,
    faUserCircle
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

export default function NavBar() {
    return (
        <AppBar position="static">
            <Toolbar>
                <Button>
                    <Link to="/">
                        <FontAwesomeIcon icon={faHome} />
                    </Link>
                </Button>
                <Button>
                    <Link to="/about">About</Link>
                </Button>
                <Button>
                    <Link to="/user">
                        <FontAwesomeIcon icon={faUserCircle} />
                    </Link>
                </Button>
                <Button>
                    <Link to="/logout">
                        <FontAwesomeIcon icon={faSignOutAlt} />
                    </Link>
                </Button>
            </Toolbar>
        </AppBar>
    );
}
