import React, {useState, useEffect} from "react";
import { Button, MenuItem, Checkbox, FormControlLabel, Select, Grid } from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import config from "../config";
import axios from "axios";
import { displayError } from "../utils/swal";

const useStyles = makeStyles(theme => ({
    form: {
        width: "70%",
        marginTop: theme.spacing(1)
    },
    submit: {
        margin: theme.spacing(3, 0, 2)
    }
}));



export default function User() {
    const classes = useStyles();
    const [formData, setFormData] = useState({
        carType: 0,
        publiBikeAccess: false,
    });

    useEffect(() => {
        const fetchData = async () => {
            const instance = axios.create({
                baseURL: config.host + config.endpoint,
                timeout: 1500,
            });
            try {
                const { data } = await instance.get(`/users/details?username=${localStorage.getItem("username")}`);
                console.log(data)
                setFormData(
                    Object.assign({}, formData, { carType: data.carType, publiBikeAccess: data.publiBikeAccess })
                );
            } catch (e) {
                displayError(e);
            }
        }
        fetchData();
    }, []);

    const handleSubmit = async e => {
        e.preventDefault();
        const instance = axios.create({
            baseURL: config.host + config.endpoint,
            timeout: 1500,
        });
        try {
            console.log(formData)
            const { data } = await instance.put("/users/settings", {
                username: localStorage.getItem("username"), 
                carType: formData.carType,
                publiBikeAccess: formData.publiBikeAccess
            });
            console.log(data)
        } catch (e) {
            displayError(e);
        }
    };

    const handleChange = (e) =>
        setFormData(
            Object.assign({}, formData, { [e.target.name]: e.target.value })
        );
    
        const checkboxChange = (e) =>
        setFormData(
            Object.assign({}, formData, { [e.target.name]: e.target.checked })
        );

    return (
        <Grid container component="main">
            <Grid item xs={false} sm={4} md={8} >
        <form className={classes.form} noValidate onSubmit={handleSubmit}>
            <Select
                fullWidth
                variant="outlined"
                name="carType"
                onChange={handleChange}
                defaultValue={formData.carType}
            >
                <MenuItem value={0}>I don't have a car</MenuItem>
                <MenuItem value={1}>I have a car</MenuItem>
                <MenuItem value={2}>I have an electric car</MenuItem>
            </Select>
                <FormControlLabel
                    control={
                        <Checkbox
                            checked={formData.publiBikeAccess}
                            color="primary"
                            name="publiBikeAccess"
                            onChange={checkboxChange}
                        />
                    }
                    label="I have a public bike account"
                />
            <Button
                type="submit"
                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
            >
                Change 
            </Button>
        </form>
        </Grid>
        </Grid>
    );
}
