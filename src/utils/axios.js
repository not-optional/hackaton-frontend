import axios from "axios";
import config from "../../package.json"

let instance = null;

export function getAxios() {
    const isDev = process.env.NODE_ENV !== 'production';
    if (!instance) {
        instance = axios.create({
            baseURL: (isDev && config.proxy ? config.proxy : window.location.origin) + config.endpoint,
            timeout: 1000,
        });
    }
    // instance.defaults.headers.common['Authorization'] = localStorage.token ? "Bearer " + localStorage.token : undefined;
    return instance
}
