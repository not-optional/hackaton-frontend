import swal from "sweetalert2";

export function displayError(e) {
    if (!e) return;
    
    if (!e.response) {
        return swal.fire({
            icon: "error",
            title: "Error!",
            text: e.message || "Something went wrong with our system! Try again later..."
        })
    }
    
    if (!e.response.data || !e.response.data.message) {
        e.response.data = {message: "No details were provided"};
    }
    
    swal.fire({
        icon: 'error',
        title: 'Error!',
        text: `${e.response.statusText} (${e.response.status}): ${e.response.data.message}`
    })
}
