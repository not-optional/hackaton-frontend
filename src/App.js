import React from "react";
import "./style.sass";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import { blue } from "@material-ui/core/colors";
import { CssBaseline } from "@material-ui/core";
import { BrowserRouter as Router } from "react-router-dom";
import AppRouter from "./AppRouter";

const theme = createMuiTheme({
    palette: {
        primary: blue,
        secondary: { main: "#757575" },
        type: "dark"
    }
});

function App() {
    return (
        <ThemeProvider theme={theme}>
            <CssBaseline>
                <div className="App">
                    <Router>
                        <AppRouter />
                    </Router>
                </div>
            </CssBaseline>
        </ThemeProvider>
    );
}

export default App;
